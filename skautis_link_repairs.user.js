// ==UserScript==
// @id             skautis link repairs
// @name           skautis link repairs
// @version        1.1
// @namespace      skautis  
// @author         schmela
// @description    upravuje odkazy na jednotky ve skautISu tak, aby vedly rovnou na seznam clenu
// @include        https://is.skaut.cz/*
// @exclude        https://is.skaut.cz/Login/*
// @require        http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @grant          GM_xmlhttpRequest
// ==/UserScript==




// ----------------------------------
// POPIS SCRIPTU
// ----------------------------------
//
// - predpokladejme, ze Moje jednotka je oddil, potom
// - vsechny odkazy na moji jednotku misto tabu "Zakladni info" budou odkazovat na tab "Clenove"
// - funguje i na primo podrizene jednotky, tj. na odkazy druzin
// - funguje i kdyz jsem v primo podrizene jednotce, tj jsem v druzine, tak odkaz na oddil je zmenen
//
// ----------------------------------



this.$ = this.jQuery = jQuery.noConflict(true);


// funkce posle xmlHttpRequest, aby zjistila ID moji jednotky, jeji jmeno a cislo a to vse nastavi do lokalniho uloziste
function set_localstorage_moje_jednotka() {
  var targURL = '/Junak/OrganizationUnit/UnitDetail.aspx';
  // http HEAD requets na targURL me presmeruje na adresu me jednotky ktera ma tvar
  // https://is.skaut.cz/Junak/OrganizationUnit/UnitDetail.aspx?ID=23211
  GM_xmlhttpRequest({
    synchronous: false,
    url: targURL,
    method: 'GET',
    headers: {
      'Cookie': document.cookie
    },
    onload: function (response) {
      
      // vypreparujeme ID z finalni adresy
      var re_ID = /.*ID=([0-9]*)/
      uID = response.finalUrl.match(re_ID) [1];
      
      // ulozime si ID moji jednotky do lokalniho uloziste
      localStorage.setItem('moje_jednotka_ID', uID);      
      
      // zparsuju si prisle html
      var parsed  = $.parseHTML(response.responseText); 
      parsed = $('<div />').append(parsed);
      
      // vytahnu si z toho jmeno jednotky a nastavim do lokalniho uloziste
      jmeno_jednotky = parsed.find('#ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder_Unit').html();
      localStorage.setItem('moje_jednotka_jmeno_jednotky', jmeno_jednotky);      
      
      // vytahnu si cislo jednotky
      cislo_jednotky = parsed.find('#ctl00_ctl00_ContentPlaceHolder1_ContentPlaceHolder_RegistrationNumber').html();

      // z cisla vyriznu cast za posledni teckou a ulozim ji do lokalniho uloziste
      var re_posledni_cislo = /.*\.(.*?)$/
      posledni_cislo = cislo_jednotky.match(re_posledni_cislo) [1];        
      localStorage.setItem('moje_jednotka_posledni_cislo', posledni_cislo);
      
      replace_links();

    }
  });
} 

// funkce vytahne z localstorage udaje a aplikuje je na strance
function replace_links(){

  // vytahneme veci z lokalniho uloziste
  uID = localStorage.moje_jednotka_ID;
  jmeno_jednotky = localStorage.moje_jednotka_jmeno_jednotky;
  posledni_cislo = localStorage.moje_jednotka_posledni_cislo;

  // NAHRAZENI V TABU "Moje jednotka"
  // ---------------------------------

  var old_link;
  if ((old_link = document.getElementById('ctl00_Menu_LoginViewNavigation_HyperLinkMyUnit2')) != null)
     old_link['href'] = 'https://is.skaut.cz/Junak/OrganizationUnit/Unit/Membership.aspx?ID=' + uID;
  if ((old_link = document.getElementById('ctl00_Menu_LoginViewNavigation_HyperLinkMyUnit')) != null)
     old_link['href'] = 'https://is.skaut.cz/Junak/OrganizationUnit/Unit/Membership.aspx?ID=' + uID;
  if ((old_link = document.getElementById('ctl00_ctl00_Menu_LoginViewNavigation_HyperLinkMyUnit')) != null)
     old_link['href'] = 'https://is.skaut.cz/Junak/OrganizationUnit/Unit/Membership.aspx?ID=' + uID;
  if ((old_link = document.getElementById('ctl00_ctl00_Menu_LoginViewNavigation_HyperLinkMyUnit2')) != null)
     old_link['href'] = 'https://is.skaut.cz/Junak/OrganizationUnit/Unit/Membership.aspx?ID=' + uID;  


  // NAHRAZENI V LEVYM MENICKU
  // ----------------------------

  var aktualni_jednotka;   
  if ((aktualni_jednotka = document.getElementById('ctl00_ctl00_ContentPlaceHolder1_UnitTreeNavigation_LabelActualUnit')) != null) {

    // AKTUALNI JEDNOTKA - nahradime vsechny linky na podradne jednotky

    // menim jenom tehda, kdyz jsem ve svem oddile
    if (aktualni_jednotka.innerHTML == (posledni_cislo + " - " + jmeno_jednotky) ) {
      var Atags = $('.lower').find("a");
      for(i=0;i<Atags.length;i++)
      {      
        Atags[i]["href"] = Atags[i]["href"].replace("UnitDetail","Unit/Membership");
      }
    }       
    else {

      // NADRIZENA JEDNOTKA - jsem v druzine -> zmenim linky co vedou zpet na oddil

      var nadrazena_jednotka;
      if ((nadrazena_jednotka = document.getElementById('ctl00_ctl00_ContentPlaceHolder1_UnitTreeNavigation_RepeaterUpper_ctl00_HyperLink1')) != null) {
        nadrazena_jednotka = nadrazena_jednotka.innerHTML;

        // menim jenom kdyz jsem v druzine sveho oddilu
        if ((nadrazena_jednotka.indexOf(posledni_cislo) > -1) && (nadrazena_jednotka.indexOf(jmeno_jednotky) > -1)) {
          var Atags = $('.upper').find("a");
          for(i=0;i<Atags.length;i++)
          {
            Atags[i]["href"] = Atags[i]["href"].replace("UnitDetail","Unit/Membership");
          }

        }
      }                 
    }  
  }  
}


// ----------------
// MAIN
// ----------------

// pokud v lokalnim ulozisti neni ID moji jednotky, je potreba ho zjistit
if (!(localStorage.moje_jednotka_ID)) {
  //console.log("nenalezeno, nacitame znovu");
  set_localstorage_moje_jednotka();
}
else {
  //console.log("nalezeno ok");
  replace_links();
}

